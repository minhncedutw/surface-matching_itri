'''
    File name: HANDBOOK
    Author: minhnc
    Date created(MM/DD/YYYY): 12/10/2018
    Last modified(MM/DD/YYYY HH:MM): 12/10/2018 9:43 AM
    Python Version: 3.6
    Other modules: [None]

    Copyright = Copyright (C) 2017 of NGUYEN CONG MINH
    Credits = [None] # people who reported bug fixes, made suggestions, etc. but did not actually write the code
    License = None
    Version = 0.9.0.1
    Maintainer = [None]
    Email = minhnc.edu.tw@gmail.com
    Status = Prototype # "Prototype", "Development", or "Production"
    Code Style: http://web.archive.org/web/20111010053227/http://jaynes.colorado.edu/PythonGuidelines.html#module_formatting
'''

#==============================================================================
# Imported Modules
#==============================================================================
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import argparse
import os.path
import sys
import time
import numpy as np

os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
os.environ["CUDA_VISIBLE_DEVICES"] = "0"  # The GPU id to use, usually either "0" or "1"

from open3d import *

#==============================================================================
# Constant Definitions
#==============================================================================

#==============================================================================
# Function Definitions
#==============================================================================
from data_loader import PartDataset
from data_loader import load_ply
from points_visualization import visualize
from surface_matching import draw_registration_result, preprocess_point_cloud, execute_global_registration, refine_registration
from surface_matching import rotationMatrixToEulerAngles

#==============================================================================
# Main function
#==============================================================================
def main(argv=None):
    print('Hello! This is Surface-Matching testing program')

    # root = 'E:/PROJECTS/NTUT/PointNet/pointnet1_pytorch/DATA/Shapenet/shapenetcore_partanno_segmentation_benchmark_v0'
    # data_tes = PartDataset(root=root, num_points=4096, categories=['tools'], training=True, balanced=True,
    #                        shuffle=True, seed=0, offset=0)
    # points, labels = data_tes[0]
    # visualize(x=points[:, 0], y=points[:, 1], z=points[:, 2], label=labels, point_radius=0.0008)  # visualize ground truth

    '''
    # Load and Visualize model and scene
    '''
    pc_model = load_ply(path='./test/1.ply', num_points=-1)
    pc_scene = load_ply(path='./test/2.ply', num_points=-1)
    visualize(x=pc_model[:, 0], y=pc_model[:, 1], z=pc_model[:, 2], label=np.ones(len(pc_model)), point_radius=0.0008)
    visualize(x=pc_scene[:, 0], y=pc_scene[:, 1], z=pc_scene[:, 2], label=np.ones(len(pc_scene)), point_radius=0.0008)

    '''
    # Surface-Matching
    '''
    # Convert numpy array to point cloud type
    source = PointCloud()
    source.points = Vector3dVector(pc_scene)
    target = PointCloud()
    target.points = Vector3dVector(pc_model)
    voxel_size = 0.001

    # downsample data
    source_down, source_fpfh = preprocess_point_cloud(source, voxel_size)
    target_down, target_fpfh = preprocess_point_cloud(target, voxel_size)
    draw_registration_result(source, target, np.identity(4)) # visualize point cloud

    # 1st: gross matching(RANSAC)
    result_ransac = execute_global_registration(source_down, target_down, source_fpfh, target_fpfh, voxel_size)
    print(result_ransac)
    draw_registration_result(source_down, target_down, result_ransac.transformation)

    # 2nd: fine-tune matching(ICP)
    result_icp = refine_registration(source, target, voxel_size, result_ransac)
    print(result_icp)
    draw_registration_result(source, target, result_icp.transformation)

    # Transformation(Rotation angles)
    print('Theta x, Theta y, Theta z:(in Degree) ')
    print(rotationMatrixToEulerAngles(result_icp.transformation[:3, :3]) / np.pi * 180)


if __name__ == '__main__':
    main()
