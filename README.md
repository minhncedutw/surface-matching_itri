# PointNet for ITRI project using basic-pytorch #

> Build PointNet semantic segmentation network on basic-pytorch framework for ITRI project.

### What is this repository for? ###

* Quick summary: Build PointNet point-wise classification(segmentation) deep network for ITRI project. Using basic pytorch.
* Version: 1.0.0

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact